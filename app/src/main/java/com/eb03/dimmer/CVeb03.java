package com.eb03.dimmer;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

/**
 * Classe View permettant l'utilisation d'une roue afin de renvoyer une valeur entre 0 et 100
 */
public class CVeb03 extends View {

    // Valeur du customView
    private float mAngle = 0;
    private float mValue = 0;

    private boolean mDoubleClick = false;
    private boolean mDisableMove = false;
    private boolean mRotationLock = true;

    // attributs de valeur
    private float mMin = 0;
    private float mMax = 100;

    // attribut d'activation
    private boolean mEnabled = true;


    // référence vers le listener
    private CVeb03ChangeListener mCVeb03ChangeListener = null;


    private final static float MIN_OUTCIRCLE_DIAMETER = 250;
    private final static float MIN_MIDDLECIRCLE_DIAMETER = MIN_OUTCIRCLE_DIAMETER-20;
    private final static float MIN_INCIRCLE_DIAMETER = MIN_OUTCIRCLE_DIAMETER*40/100;


    // Dimensions par défaut
    private final static float DEFAULT_OUTCIRCLE_DIAMETER = 400;
    private final static float DEFAULT_MIDDLECIRCLE_DIAMETER = DEFAULT_OUTCIRCLE_DIAMETER-20;
    private final static float DEFAULT_INCIRCLE_DIAMETER= DEFAULT_OUTCIRCLE_DIAMETER*40/100;
    private  final static  float DEFAULT_THICK_WIDTH=5;
    private  final  static  float DEFAULT_THIN_WIDTH=2;


    //attributs de dimension (en pixels)
    private float mOutCirleDiameter;
    private float mMiddleCirleDiameter;
    private float mInCircleDiameter;

    //attributs graduations

    private float mOrigineThick;
    private float mLenghtThick;
    private float mOrigineThin;
    private float mLenghtThin;
    private  float mThickWidth;
    private  float mThinWidth;


    // attributs de couleur
    private int mDisabledColor;
    private int mMiddleCircleColor;
    private int mValueCircleColor;
    private int mInCircleColor;
    private int mOutCircleColor;
    private int mThickColor;
    private int mThinColor;

    // attributs de pinceaux
    private Paint mInCirclePaint;
    private Paint mValueCirclePaint;
    private Paint mMiddleCirclePaint;
    private Paint mOutCirclePaint;
    private Paint mThickPaint;
    private Paint mThinPaint;

    private float dpToPixel(float dp){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,dp,getResources().getDisplayMetrics());
    }

    public CVeb03(Context context) {
        super(context);
    }

    public CVeb03(Context context, @Nullable AttributeSet attrs){
        super(context, attrs);
        init(context,attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mInCirclePaint = new Paint();
        mMiddleCirclePaint = new Paint();
        mValueCirclePaint = new Paint();
        mOutCirclePaint = new Paint();
        mThickPaint =new Paint();
        mThinPaint =new Paint();

        mInCircleColor = ContextCompat.getColor(context,R.color.design_default_color_secondary);
        mMiddleCircleColor = ContextCompat.getColor(context,R.color.design_default_color_primary);
        mValueCircleColor = ContextCompat.getColor(context,R.color.purple_200);
        mOutCircleColor = ContextCompat.getColor(context,R.color.black);
        mThickColor = ContextCompat.getColor(context,R.color.black);
        mThinColor = ContextCompat.getColor(context,R.color.design_default_color_secondary);
        mDisabledColor = ContextCompat.getColor(context,R.color.grey);


        mOutCirleDiameter = dpToPixel(DEFAULT_OUTCIRCLE_DIAMETER);
        mInCircleDiameter = dpToPixel(DEFAULT_INCIRCLE_DIAMETER);
        mMiddleCirleDiameter = dpToPixel(DEFAULT_MIDDLECIRCLE_DIAMETER);
        mThickWidth = dpToPixel(DEFAULT_THICK_WIDTH);
        mThinWidth = dpToPixel(DEFAULT_THIN_WIDTH);
        mOrigineThin= (mMiddleCirleDiameter-mInCircleDiameter)/3;
        mLenghtThin=(mMiddleCirleDiameter-mInCircleDiameter)/3;
        mOrigineThick=(mMiddleCirleDiameter-mInCircleDiameter)/4;
        mLenghtThick=(mMiddleCirleDiameter-mInCircleDiameter)/2;
        mThickPaint.setStrokeWidth(mThickWidth);
        mThinPaint.setStrokeWidth(mThinWidth);

        //Récupération des attributs xml si présents
        if(attrs !=null){
            TypedArray attr = context.obtainStyledAttributes(attrs,R.styleable.CVeb03,0,0);
            mOutCirleDiameter= attr.getDimension(R.styleable.CVeb03_OutcircleDiameter,mOutCirleDiameter);
            mOutCircleColor= attr.getColor(R.styleable.CVeb03_OutcircleDiameter,mOutCircleColor);
            mInCircleColor= attr.getColor(R.styleable.CVeb03_InCircleColor,mInCircleColor);
            mMiddleCircleColor= attr.getColor(R.styleable.CVeb03_MiddleCircleColor,mMiddleCircleColor);
            mValueCircleColor= attr.getColor(R.styleable.CVeb03_ValueCircleColor,mValueCircleColor);
            mThickColor= attr.getColor(R.styleable.CVeb03_ThickBarColor,mThickColor);
            mThinColor= attr.getColor(R.styleable.CVeb03_ThinBarColor,mThinColor);
            mEnabled = attr.getBoolean(R.styleable.CVeb03_enabled,mEnabled);
            attr.recycle();
        }



        if(mEnabled){
            mInCirclePaint.setColor(mInCircleColor);
            mValueCirclePaint.setColor(mValueCircleColor);
            mOutCirclePaint.setColor(mOutCircleColor);
            mMiddleCirclePaint.setColor(mMiddleCircleColor);
            mThickPaint.setColor(mThickColor);
            mThinPaint.setColor(mThinColor);
        }else{
            mInCirclePaint.setColor(mDisabledColor);
            mValueCirclePaint.setColor(mDisabledColor);
            mOutCirclePaint.setColor(mDisabledColor);
            mMiddleCirclePaint.setColor(mDisabledColor);
            mThickPaint.setColor(mDisabledColor);
            mThinPaint.setColor(mDisabledColor);
        }

        mMiddleCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mValueCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mInCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mThickPaint.setStyle(Paint.Style.STROKE);
        mThinPaint.setStyle(Paint.Style.STROKE);
        mThickPaint.setStrokeCap(Paint.Cap.ROUND);
        mThinPaint.setStrokeCap(Paint.Cap.ROUND);

    }

    private void adaptDims(){
        float pt = getPaddingTop();
        float pb = getPaddingBottom();
        float pl = getPaddingLeft();
        float pr = getPaddingRight();

        final float minOutCircleDiameter = dpToPixel(MIN_OUTCIRCLE_DIAMETER);
        final float minInCircleDiameter = dpToPixel(MIN_INCIRCLE_DIAMETER);
        final float minMiddleCircleDiameter = dpToPixel(MIN_MIDDLECIRCLE_DIAMETER);
        final float minValueCircleDiameter = dpToPixel(MIN_MIDDLECIRCLE_DIAMETER);


        /********** Traitement de la largeur du Slider ****************/
        // le plus petit curseur excède la dimension affectée (annulation des paddings + réduction du curseur)
        if(minOutCircleDiameter > getWidth()){
            mOutCirleDiameter = getWidth();
            mInCircleDiameter= (getWidth()*40/100);
            mMiddleCirleDiameter=getWidth()-dpToPixel(20);
            pl = 0;
            pr = 0;
            mOrigineThin= (mMiddleCirleDiameter-mInCircleDiameter)/4;
            mLenghtThin=(mMiddleCirleDiameter-mInCircleDiameter)/3;
            mOrigineThick=(mMiddleCirleDiameter-mInCircleDiameter)/5;
            mLenghtThick=(mMiddleCirleDiameter-mInCircleDiameter)*40/100;
        }
        // le plus petit curseur + padding excèdent la dimension affectée
        else if (minOutCircleDiameter + pl + pr >getWidth()){

            mOutCirleDiameter = minOutCircleDiameter;
            mInCircleDiameter= minInCircleDiameter;
            mMiddleCirleDiameter=minMiddleCircleDiameter;
            float ratio = (getWidth()-minOutCircleDiameter)/(pl+pr);
            pl *=ratio;
            pr *=ratio;
            mOrigineThin= (mMiddleCirleDiameter-mInCircleDiameter)/4;
            mLenghtThin=(mMiddleCirleDiameter-mInCircleDiameter)/3;
            mOrigineThick=(mMiddleCirleDiameter-mInCircleDiameter)/5;
            mLenghtThick=(mMiddleCirleDiameter-mInCircleDiameter)*40/100;
        }
        else if(mOutCirleDiameter + pl + pr >getWidth()){

            mOutCirleDiameter = getWidth()- pl - pr;
            mInCircleDiameter= (getWidth()- pl - pr)*40/100;
            mMiddleCirleDiameter= getWidth()- pl - pr -dpToPixel(20);
            mOrigineThin= (mMiddleCirleDiameter-mInCircleDiameter)/4;
            mLenghtThin=(mMiddleCirleDiameter-mInCircleDiameter)/3;
            mOrigineThick=(mMiddleCirleDiameter-mInCircleDiameter)/5;
            mLenghtThick=(mMiddleCirleDiameter-mInCircleDiameter)*40/100;
        }



        setPadding((int)pl,(int)pt,(int)pr,(int)pb);
    }

    /******************************************************************************************/
    /*                            affichage des éléments                      */

    /******************************************************************************************/
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Point center;
        adaptDims();
        center= toPos();
        canvas.drawCircle(center.x,center.y,mOutCirleDiameter/2 ,mOutCirclePaint);
        canvas.drawCircle(center.x,center.y, mMiddleCirleDiameter/2,mMiddleCirclePaint);
        RectF ValueCircle=new RectF();
        ValueCircle.set((int)((mOutCirleDiameter/2)+getPaddingLeft()-(mMiddleCirleDiameter/2)),(int)((mOutCirleDiameter/2)+getPaddingTop()-(mMiddleCirleDiameter/2)),(int)((mOutCirleDiameter/2)+getPaddingRight()+(mMiddleCirleDiameter/2)),(int)((mOutCirleDiameter/2)+getPaddingBottom()+(mMiddleCirleDiameter/2)));
        canvas.drawArc(ValueCircle,270,mAngle,true,mValueCirclePaint);
        canvas.drawCircle(center.x,center.y, mInCircleDiameter/2,mInCirclePaint);
        double AngleGraduation;

        Point p1,p2;
        int k=0;
        for(double i=0; i<2*Math.PI;i=i+2*(Math.PI)/16){

            if ((i%(Math.PI/4))<0.01) {

                AngleGraduation=0;
            }
            else {
                AngleGraduation = i%(Math.PI/4);
            }

            if (AngleGraduation ==0) {
                p1=toPosLine(i,mOrigineThick);
                p2=toPosLine(i,mLenghtThick);
                canvas.drawLine(p1.x,p1.y,p2.x,p2.y,mThickPaint);
            }
            else {
                 p1=toPosLine(i,mOrigineThin);
                 p2=toPosLine(i,mLenghtThin);
                 canvas.drawLine(p1.x,p1.y,p2.x,p2.y,mThinPaint);

             }
        }

    }
    private Point toPos(){
        int x,y;
        x = (int)((mOutCirleDiameter/2)+getPaddingLeft());
        y = (int)((mOutCirleDiameter/2)+getPaddingTop());
        return new Point(x,y);
    }
    private  Point toPosLine(double i, float value){
        int x, y;
        x= (int)(Math.cos(i)*((mInCircleDiameter/2)+value))+(int)(mOutCirleDiameter/2)+getPaddingLeft();
        y= (int)(Math.sin(i)*((mInCircleDiameter/2)+value))+(int)(mOutCirleDiameter/2)+getPaddingTop();
        return new Point(x,y);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int suggestedWidth,suggestedHeight; // dimensions souhaitées par le slider
        int width,height; // dimensions calculées en tenant compte des spécifications du container

        suggestedHeight = (int) dpToPixel((DEFAULT_OUTCIRCLE_DIAMETER/2)+ getPaddingTop()+ getPaddingBottom());
        suggestedWidth = (int) dpToPixel((DEFAULT_OUTCIRCLE_DIAMETER/2)+ getPaddingTop()+ getPaddingBottom());

        width = resolveSize(suggestedWidth,widthMeasureSpec);
        height = resolveSize(suggestedHeight,heightMeasureSpec);


        setMeasuredDimension(width,height);
    }
    /******************************************************************************************/
    /*                            Traitement des valeurs                      */

    /******************************************************************************************/
    public interface CVeb03ChangeListener{
        void onChange(float value);
        void onDoubleClick(float value);
    }
    public void setSliderChangeListener(CVeb03ChangeListener cVeb03ChangeListener) {
        mCVeb03ChangeListener = cVeb03ChangeListener;
    }
    //fonction permettant de calculer l'angle en fonction d'un point
    private float toAngle (Point x){
        float angle=mValue*360/100;
        float TanAngle;
        //permet d'éviter de diviser par zéro
        if((x.x ==((mOutCirleDiameter/2)+getPaddingLeft()))&&x.y<((mOutCirleDiameter/2)+getPaddingTop())){
            if (mRotationLock ){angle = 0;}
            else if (!mRotationLock ){angle = 360;}
        }
        //permet d'éviter de diviser par zéro
        else if ((x.x ==((mOutCirleDiameter/2)+getPaddingLeft()))&&x.y>((mOutCirleDiameter/2)+getPaddingTop())){
            angle = 180;
        }
        //Quart supérieur droit
        else if (((x.x >((mOutCirleDiameter/2)+getPaddingLeft()))&&x.y<((mOutCirleDiameter/2)+getPaddingTop()))&&mRotationLock ==true){
            TanAngle = ((x.x - ((mOutCirleDiameter / 2) + getPaddingLeft()))/(((mOutCirleDiameter / 2) + getPaddingTop())-x.y ));
            angle = (float) (Math.atan(TanAngle)*180/Math.PI);

        }
        //Quart inférieur droit
        else if ((x.x >((mOutCirleDiameter/2)+getPaddingLeft()))&&x.y>=((mOutCirleDiameter/2)+getPaddingTop())){
            TanAngle = (x.y -((mOutCirleDiameter / 2) + getPaddingTop()))/((x.x - ((mOutCirleDiameter / 2) + getPaddingLeft())));
            angle = 90 + (float) (Math.atan(TanAngle)*180/Math.PI);
            if (angle<=95) mRotationLock = true;
        }
        //Quart inférieur gauche
        else if ((x.x < ((mOutCirleDiameter/2)+getPaddingLeft()))&&x.y>=((mOutCirleDiameter/2)+getPaddingTop())){
            TanAngle = ((((mOutCirleDiameter / 2) + getPaddingLeft()))-x.x )/(x.y -((mOutCirleDiameter / 2) + getPaddingTop()));
            angle = 180 + (float) (Math.atan(TanAngle)*180/Math.PI);
            if (angle>=260) mRotationLock = false;
        }
        //Quart supérieur gauche
        else if (((x.x < ((mOutCirleDiameter/2)+getPaddingLeft()))&&x.y<=((mOutCirleDiameter/2)+getPaddingTop()))&& !mRotationLock){
            TanAngle = (((mOutCirleDiameter / 2) + getPaddingTop())-x.y)/((((mOutCirleDiameter / 2) + getPaddingLeft()))-x.x );
            angle = 270 + (float) (Math.atan(TanAngle)*180/Math.PI);
        }
        //Fixation de l'état lors qu'on passe à 100 dans le sens horaire
        else if (((x.x >((mOutCirleDiameter/2)+getPaddingLeft()))&&x.y<((mOutCirleDiameter/2)+getPaddingTop()))&& !mRotationLock){
            angle = 360;

        }
        //Fixation de l'état lors qu'on passe à 0 dans le sens anti horaire
        else if (((x.x < ((mOutCirleDiameter/2)+getPaddingLeft()))&&x.y<=((mOutCirleDiameter/2)+getPaddingTop()))&& mRotationLock){
            angle = 0;
        }


        return angle;


    }
    /******************************************************************************************/
    /*                            Gestion des events                     */

    /******************************************************************************************/
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()){
            case MotionEvent.ACTION_MOVE:
                if(!mDisableMove){
                    mAngle = toAngle(new Point((int)event.getX(),(int)event.getY()));
                    mValue = 100*mAngle/360;

                    if(mCVeb03ChangeListener != null) mCVeb03ChangeListener.onChange(mValue);
                    invalidate();}
                break;
            case  MotionEvent.ACTION_UP:
                mRotationLock=true;
            case MotionEvent.ACTION_DOWN :

                if(mDoubleClick){
                    mDisableMove = true;
                    postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mDisableMove = false;
                        }
                    },200);
                    mValue = mMin;
                    mAngle= 0;
                    if(mCVeb03ChangeListener != null) mCVeb03ChangeListener.onDoubleClick(mValue);
                    invalidate();
                }else{
                    mDoubleClick = true;
                    postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mDoubleClick = false;
                        }
                    },300);
                }


                invalidate();
                break;
            default:


        }


        return true;
    }

    /******************************************************************************************/
    /*                            Sauvegarde et restauration de l'état                        */

    /******************************************************************************************/

    private class SavedState extends BaseSavedState {

        private float mSavedValue;


        // utilisé par onSaveInstanceState
        public SavedState(Parcelable superState) {
            super(superState);
            mSavedValue = mAngle;
        }

        // constructeur utilisé par le Creator pour recréer la classe SavedState
        private SavedState(Parcel in) {
            super(in);
            mSavedValue = in.readFloat();
        }

        public final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            @Override
            public SavedState[] newArray(int i) {
                return new SavedState[0];
            }
        };

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeFloat(mSavedValue);
        }
    }

    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {
        return new SavedState(super.onSaveInstanceState());
    }


    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        } else {
            super.onRestoreInstanceState(((SavedState) state).getSuperState());
            mAngle = ((SavedState) state).mSavedValue;
        }
    }

}
