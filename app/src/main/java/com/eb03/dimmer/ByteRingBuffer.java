package com.eb03.dimmer;

import java.util.Arrays;

public class  ByteRingBuffer {

    private int BufferSize ;
    private int readIndex;
    private int writeIndex ;
    public byte[] RingBuffer;
    public boolean OVERFLOW;
    public  boolean UNDERFLOW;

    /**
     * Creation d'un buffer circulaire
     * @param value nombre de byte du buffer
     */
    public ByteRingBuffer(int value){
        this.BufferSize=value;
        this.readIndex=0;
        this.writeIndex = 0;
        this.RingBuffer = new byte[BufferSize];
        this.OVERFLOW= false;
        this.UNDERFLOW=true;
    }


    /**
     * Methode ajoutant un byte au writeIndex du buffer
     * @param value
     */
    public void put(byte value){
        if (writeIndex== (readIndex+BufferSize-1)%BufferSize) { OVERFLOW=true;
        } else {
            RingBuffer[writeIndex] = value;
            writeIndex = (writeIndex + 1) % BufferSize;
            UNDERFLOW=false;
        }
    }

    /**
     * Méthode ajoutant un tableau de byte débutant au writeIndex
     * @param value
     */
    public void put(byte[] value){
        if ((value.length>(BufferSize+readIndex-writeIndex)%BufferSize)&&UNDERFLOW==false) {OVERFLOW=true;
        } else {
            if(OVERFLOW==false){
                for (int i = 0; i < value.length; i++) {
                        RingBuffer[(writeIndex + i)%BufferSize] = value[i];
                }
                writeIndex= ((writeIndex+value.length+1)%BufferSize);
                UNDERFLOW=false;
            }
        }
    }

    /**
     * Methode permettant la lecture d'un byte du buffer à l'index readIndex
     *
     * @return un byte du tableau si UNDERFLOW est faux, si vrai, le tableau et vide, get retournera 9.
     *
     */
    public byte get(){

        if(OVERFLOW){OVERFLOW=false;}
        if (readIndex!=writeIndex){
            readIndex=(readIndex+1)%BufferSize;
            if(readIndex==writeIndex){UNDERFLOW=true; }
            return RingBuffer[(BufferSize + readIndex-1)%BufferSize];
        } else {
            UNDERFLOW=true;
        }
        return 9;
    }

    /**
     * Methode permettant de lire l'ensemble du buffer
     * @return le contenu du buffer sous un tableau de byte
     */
    public byte[] getAll(){
        readIndex= (readIndex+BufferSize-1)%BufferSize;
        if(OVERFLOW){OVERFLOW=false;}
        return RingBuffer;
    }

    /**
     * Methode indiquant le nombre de byte à lire entre l'index de lecture et l'index d'ecriture
     * @return nombre de byte non lus
     */
    public  int bytestoRead(){
        if (writeIndex>=readIndex){

            return (writeIndex-readIndex+BufferSize)%BufferSize;

        } else{

            return writeIndex-readIndex+BufferSize;
        }


    }

    /**
     * Methode qui renvoie l'ensemble des paramètres du buffer
     * @return l'état des variables
     */
    @Override
    public String toString() {
        return "ByteRingBuffer{" +
                "BufferSize=" + BufferSize +
                ", readIndex=" + readIndex +
                ", writeIndex=" + writeIndex +
                ",OVERFLOW="+OVERFLOW +
                ",UNDERFLOW="+UNDERFLOW +
                ", RingBuffer=" + Arrays.toString(RingBuffer) +
                '}';
    }
}
