package com.eb03.dimmer;

import java.util.Arrays;

public class OscilloManager
{   private  Transceiver mTransceiver;
    /** Constructeur privé */
    private OscilloManager()
    {}

    /** Instance unique pré-initialisée */
    private static OscilloManager INSTANCE = new OscilloManager();

    /** Point d'accès pour l'instance unique du singleton */
    /**
     * Point d'accès pour l'instance unique du singleton
     * @return
     */
    public static OscilloManager getInstance()
    {
        return INSTANCE;
    }

    /**
     * Méthode insertion de l'identifiant de commande dans le payload et envoi par tranceiver générique
     * @param value valeur de la commande
     */
    public  void  setCalibrationDutyCycle(float value){
        byte[] c = new byte[2];
        c[0] = 0X0A;
        c[1]= (byte)value;

        mTransceiver.send(c);
    }

    /**
     * Methode liant le tranceiver générique au tranceiver utilisé
     * @param transceiver tranceiver utilisé
     */
    public    void attachTransceiver(Transceiver transceiver){
        mTransceiver = transceiver;
    }
}
