package com.eb03.dimmer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSeekBar;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
/** la classe BTManager est une extension de la classe tranceiver,
 * @see Transceiver
 * Elle est responsable de la demande d'ouverture du socket et du lancement des threads d'interactions bluetooth
 */

public class BTManager extends Transceiver {

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    public static final int STATE_NOT_CONNECTED = 0; // non connecté
    public static final int STATE_CONNECTING = 1;    // connexion en cours
    public static final int STATE_CONNECTED = 2;     // connecté

    private BluetoothAdapter mAdapter;

    private BluetoothSocket mSocket = null;
    private FrameProcessor mFrameProcessor = new FrameProcessor();

    private ConnectThread mConnectThread = null;
    private WritingThread mWritingThread = null;

    /**La méthode connect lance le thread de connexion
     *
     * @param id l'adresse de l'objet bluetooth
     */
    @Override
    public void connect(String id) {
        mAdapter= BluetoothAdapter.getDefaultAdapter();
        BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(id);
        disconnect();
        mConnectThread = new ConnectThread(device);
        setState(STATE_CONNECTING);
        mConnectThread.start();
    }


    @Override
    public void setState(int state) {
        super.setState(state);
    }

    @Override
    public void disconnect() {

    }

    /**
     * La méthode send indique le payload à envoyer au mWritingThread
     * @param b le payload
     */
    @Override
    public void send(byte[] b) {
        mWritingThread.write(mFrameProcessor.toFrame(b));
    }

    /**
     * Classe du Thread de connexion au device
     */
    private class ConnectThread extends Thread{


        public ConnectThread(BluetoothDevice device) {
            //BluetoothSocket socket = null;

            try {
                mSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            mAdapter.cancelDiscovery();

            try {
                mSocket.connect();
            } catch (IOException e) {
                disconnect();
            }
            mConnectThread = null;
            startReadWriteThreads();

        }
    }

    /**
     * Classe du thread de lancement des threads de lecture et d'ecriture
     *
     */
    private void startReadWriteThreads(){
        // instanciation d'un thread de lecture

        mWritingThread = new WritingThread(mSocket);
        Log.i("ConnectThread","Thread WritingThread lancé");
        mWritingThread.start();
        setState(STATE_CONNECTED);
    }

    /**
     * Classe du Thread d'écriture vers le device
     */
    private class WritingThread extends Thread{
        private OutputStream mOutStream;
        private ByteRingBuffer mByteRingBuffer ;

        /**
         * méthode instancie un buffer circulaire et lie le socket de communication à OutputStream
         * @param mSocket
         */
        public WritingThread(BluetoothSocket mSocket) {
            mByteRingBuffer= new ByteRingBuffer(128);
            try {
                mOutStream = mSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Methode qui envoie le contenu du buffer si il n'est pas vide
         */
        @Override
        public void run() {
            while(mSocket != null){
                if (mByteRingBuffer.bytestoRead()!=0){
                try {
                   mOutStream.write(mByteRingBuffer.get());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }}
        }

        /**
         * Methode remplissant le buffer circulaire avec une trame
         * @param value trame à stocker en attendant l'envoi
         */
        void write (byte[] value){
            mByteRingBuffer.put(value);
        }
        // déclarer une ref vers un buffer circulaire


    }






}
