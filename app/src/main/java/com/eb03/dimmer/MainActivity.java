package com.eb03.dimmer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import static android.os.Build.VERSION.SDK_INT;

public class MainActivity extends AppCompatActivity {

    private final static int BT_CONNECT_CODE = 1;
    private final static int PERMISSIONS_REQUEST_CODE = 0;
    private final static String[] BT_DANGEROUS_PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    private TextView mStatus;
    private CVeb03 mCVeb03;
    private  float mValue;
    private TextView mTv;
    private  BTManager mBTmanager=new BTManager();
    private OscilloManager mOscilloManager=OscilloManager.getInstance();

    /**
     * Initialisation des éléments de l'interface
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCVeb03= findViewById(R.id.cveb03);
        mStatus = findViewById(R.id.status);
        mTv=findViewById(R.id.tv);
        verifyBtRights();
        mCVeb03.setSliderChangeListener(new CVeb03.CVeb03ChangeListener() {
            /**
             * Mise à jour des éléments de l'interface
             * @param value
             */
            @Override
            public void onChange(float value) {
                mTv.setText(String.format("%d",(int)value)+"%");
                mValue = value;
                mOscilloManager.setCalibrationDutyCycle(mValue);
            }

            /**
             * Gestion du double click de CVeb03
             * @param value
             */
            @Override
            public void onDoubleClick(float value) {
                mTv.setText(String.format("%d",(int)value)+"%");
                mOscilloManager.setCalibrationDutyCycle(value);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    /**
     * Selection de l'item du menu
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int menuItem = item.getItemId();
        switch(menuItem){
            case R.id.connect:
                Intent BTConnect;
                BTConnect = new Intent(this,BTConnectActivity.class);
                startActivityForResult(BTConnect,BT_CONNECT_CODE);
        }
        return true;
    }


    /**
     * Methode de vérification des droits
     */
    private void verifyBtRights(){
        if(BluetoothAdapter.getDefaultAdapter() == null){
            Toast.makeText(this,"Cette application nécessite un adaptateur BT",Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        if(SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_DENIED){
                requestPermissions(BT_DANGEROUS_PERMISSIONS,PERMISSIONS_REQUEST_CODE);
            }
        }

    }

    /**
     * Demande à l'utilisateur les permissions
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSIONS_REQUEST_CODE){
            if(grantResults[0] == PackageManager.PERMISSION_DENIED){
                Toast.makeText(this,"Les autorisations BT sont requises pour utiliser l'application",Toast.LENGTH_LONG).show();
                finish();
                return;
            }

        }
    }

    /**
     * Connexion au device
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case BT_CONNECT_CODE:
                if (resultCode == RESULT_OK) {
                    String address = data.getStringExtra("device");
                    mStatus.setText(address);
                    mOscilloManager.attachTransceiver(mBTmanager);
                    mBTmanager.connect(address);


                }
                break;
            default:
        }


    }

    /**
     * Enregistrer les élements nécéssaire à la sauvegarde des éléments graphique
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("key_mValue", (int) mValue);;
    }

    /**
     * Restauration des éléments graphique
     * @param savedInstanceState
     */
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mValue = savedInstanceState.getInt("key_mValue", 0);
        mTv.setText(String.format("%d",(int)mValue)+"%");
    }
}



